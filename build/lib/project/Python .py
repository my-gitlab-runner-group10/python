import json
# Python program to display all the prime numbers within an interval
def function(x):
    myList=[]

    print("Prime numbers from 2 up to " + str(x)+" are:")
    for num in range(2, x + 1):
        # all prime numbers are greater than 1
        if num > 1:
            for i in range(2, num):
                if (num % i) == 0:
                    break
            else:
                myList.append(num) 
    return(myList)        
        
#function(int(input("Enter a number :")))
